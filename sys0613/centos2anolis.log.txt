2023-01-10 21:17:28.550[INFO    ]: ====== Start ======
2023-01-10 21:17:28.550[INFO    ]: Checking if the tool is executed by root user
2023-01-10 21:17:28.550[INFO    ]: Get current OS version.
2023-01-10 21:17:28.552[INFO    ]: Version is 7.9
2023-01-10 21:17:28.553[INFO    ]: Checking the version of Anolis OS
2023-01-10 21:17:28.553[INFO    ]: Switch to Anolis OS 7.9
2023-01-10 21:17:28.553[INFO    ]: Checking required packages
2023-01-10 21:17:28.566[INFO    ]: Checking i686 packages
2023-01-10 21:17:30.787[INFO    ]: Verify before
2023-01-10 21:17:32.325[INFO    ]: Checking: distribution
2023-01-10 21:17:32.390[INFO    ]: centos-release-7-9.2009.1.el7.centos.aarch64
2023-01-10 21:17:32.391[INFO    ]: Checking: yum lock
2023-01-10 21:17:32.392[INFO    ]: Checking network environment
2023-01-10 21:17:32.594[INFO    ]: Check internal network environment is True
2023-01-10 21:17:32.594[INFO    ]: Check dnf modules
2023-01-10 21:17:32.594[INFO    ]: Looking for yumdownloader
2023-01-10 21:17:32.599[INFO    ]: Checking mysql version
2023-01-10 21:17:32.663[INFO    ]: Finding your repository directory
2023-01-10 21:17:32.664[INFO    ]: Backing up and removing old repository files...
2023-01-10 21:17:34.934[INFO    ]: Processing contentdir in repo files...
2023-01-10 21:17:34.936[INFO    ]: Backing up and removing 3rd-part repository files...
2023-01-10 21:17:34.946[INFO    ]: switch to anolis.repo
2023-01-10 21:17:34.952[INFO    ]: Removing CentOS-specific yum configuration from /etc/yum.conf ...
2023-01-10 21:17:34.953[INFO    ]: Downloading Anolis OS release package...
2023-01-10 21:17:36.252[INFO    ]: Switching old release package with Anolis OS...
2023-01-10 21:17:36.409[INFO    ]: optimise repofile
2023-01-10 21:17:36.417[INFO    ]: Installing base packages for Anolis OS...
2023-01-10 21:17:49.735[INFO    ]: process pkgs for c7
2023-01-10 21:17:55.036[INFO    ]: Distro sync
2023-01-10 21:20:22.337[INFO    ]: Process enabled modules
2023-01-10 21:20:22.337[INFO    ]: Swap centos-logos related packages with Anolis OS packages
2023-01-10 21:20:24.122[INFO    ]: Packages related to rhn are not provided by Anolis OS
2023-01-10 21:20:24.187[INFO    ]: Package python3-syspurpose is not provided by Anolis OS
2023-01-10 21:20:24.252[INFO    ]: Remove centos gpg-pubkey
2023-01-10 21:20:24.388[INFO    ]: Removing yum cache
2023-01-10 21:20:25.185[INFO    ]: Verify after
2023-01-10 21:20:25.185[INFO    ]: Sync successfully, update grub.cfg.
2023-01-10 21:20:25.761[INFO    ]: Current system is uefi, add boot option to boot manager.
2023-01-10 21:20:26.496[INFO    ]: Yum clean all
2023-01-10 21:20:32.232[INFO    ]:           4 packages installed - for details: /var/log/installed-packages.log
2023-01-10 21:20:32.243[INFO    ]:           341 packages swapped - for details: /var/log/swapped-packages.log
2023-01-10 21:20:32.244[INFO    ]:            13 packages updated - for details: /var/log/updated-packages.log
2023-01-10 21:20:32.244[INFO    ]:            4 packages reserved - for details: /var/log/reserved-packages.log
2023-01-10 21:20:32.245[INFO    ]:             3 packages removed - for details: /var/log/removed-packages.log
2023-01-10 21:20:32.245[INFO    ]: Switch complete. Anolis OS recommends rebooting this system.